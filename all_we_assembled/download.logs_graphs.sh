
for file in `find /rs2/contigs/ -type f -name "*.contigs.fasta.lz4"`
do
	srr="${file%%.*}"
	srr=$(echo $srr | rev | cut -d'/' -f 1 | rev)
	if [[ $srr != *"RR"* ]]; then
		  echo "invalid file: $file"
		  continue
	fi

	
	firsttwo=`echo $file | awk -F/ '{print $4}' | awk -F. '{print $1"."$2}'`
		
	if [ -f logs/$firsttwo.log ]
	then
		echo "logs/$firsttwo.log exists"
		continue
	fi
	
	lasttwo=`echo $file | awk -F/ '{print $4}' | awk -F. '{print $3"."$4}'`

	if [[ "$lasttwo" == "contigs.fasta" ]]
	then
		echo "$firsttwo.$lasttwo"

		k=$(bash -c "aws s3 cp s3://lovelywater/assembly/contigs/$firsttwo.assembly_graph_with_scaffolds.gfa.gz - | gunzip -c /dev/stdin | grep '^L' | tail -n +1 | head -n 1 | cut -f 6 | sed 's/M//'")

		if [[ "$k" == "" ]]
		then 
			k=$(bash -c "aws s3 cp s3://serratus-rayan/lovelywater/contigs/$firsttwo.assembly_graph_with_scaffolds.gfa.gz - | gunzip -c /dev/stdin | grep '^L' | tail -n +1 | head -n 1 | cut -f 6 | sed 's/M//'")
		fi
		if [[ "$k" == "" ]]
		then 
			k="-1"
		fi
		echo "$srr $k" > logs/$firsttwo.log 
	fi

done
