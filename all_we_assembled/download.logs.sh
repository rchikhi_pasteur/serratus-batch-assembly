
for file in `find /rs2/contigs/ -type f`
do
	srr="${file%%.*}"
	if [[ $srr != *"RR"* ]]; then
		  echo "invalid file: $file"
		  continue
	fi

	
	firsttwo=`echo $file | awk -F/ '{print $4}' | awk -F. '{print $1"."$2}'`
		
	if [ -f logs/$firsttwo.log ]
	then
		echo "logs/$firsttwo.log exists"
		continue
	fi

	aws s3 cp s3://lovelywater/assembly/contigs/$firsttwo.txt logs/ && continue
	aws s3 cp s3://lovelywater/assembly/contigs/$firsttwo.log logs/ && continue
	aws s3 cp s3://serratus-rayan/lovelywater/contigs/$firsttwo.log logs/ && continue
	aws s3 cp s3://serratus-rayan/lovelywater/contigs/$firsttwo.txt logs/ && continue
	echo "couldn't fine logs for $file ($firsttwo)"

done
