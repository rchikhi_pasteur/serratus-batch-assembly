#uri=serratus-public/assemblies/epsys_120_july21/other/
#uri=serratus-public/assemblies/phage_april21/other/
uri=serratus-public/assemblies/infernal_59_feb22/other/
#midfix=.rnaviralspades-palmfold_5k_feb22
#midfix=.metaspades-phage_april21
midfix=.rnaviralspades-infernal_59_feb22

for file in `cat to_dl.files.txt`
do
	srr="${file%%.*}"
	if [[ $srr != *"RR"* ]]; then
		  echo "invalid file: $file"
		  continue
	fi
	if  [ -f staged/$file ]; then
		continue
	fi

	aws s3 cp s3://$uri"$srr"$midfix/$file staged/
done
