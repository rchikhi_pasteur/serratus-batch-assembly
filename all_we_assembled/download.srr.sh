uri=serratus-public/assemblies/other/
mkdir -p staged

for srr in `cat to_dl.txt`
do
	if [[ $srr != *"RR"* ]]; then
		  echo "invalid srr: $srr"
		  continue
	fi
	aws s3 cp s3://$uri"$srr.coronaspades/" staged/ --recursive
done
