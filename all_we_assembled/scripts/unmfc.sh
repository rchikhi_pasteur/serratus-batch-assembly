cores=16

rm -f warnings.txt 

job() {
	f=$1
	echo $f
	decomp=${f%.*}
	
	if [ ! -f $decomp ]
	then
		/MFCompressD -o $decomp $f
	else
		echo "$decomp already exists"
	fi

	if [ -f $decomp ]
	then
		if [ -s $decomp ]
		then
			echo "$decomp exists, non-empty, original going to be deleted"
			rm -f $f
		else
			echo "warning: $decomp exists but is empty" | tee -a warnings.txt
		fi
	fi
}
export -f job

find contigs/ -name "*.mfc" | parallel -j $cores  "job {}"
