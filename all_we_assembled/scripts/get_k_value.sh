# find the k value for an accession, from logs

srr=$1
if [ -z "$srr" ]
then
	      echo "$srr is empty"
	      exit 1
fi
srrdot="$srr".

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
k=-1

for log in `find logs/ -name "*$srrdot*"`
do
	newk=$(python $SCRIPT_DIR/get_k_value.py $log)
	if [[ "$k" == "-1" ]]
	then
		k=$newk
	else
		if [[ "$k" != "$newk" ]]
		then
			echo "[warning] discordant k's found for $srr: $k != $newk"
		fi
	fi
done
echo $k
