# this script both compresses in lz4 and renames the headers appropriately

cores=2

job() {
	set -e
	f=$1

	# determine if that file has the right header type
	hd=`lz4cat $f |head -n 1`

	goodformat=0
	if [[ "$hd" == *"RR"* ]] 
	then
		goodformat=1
	fi

	# rename headers if necessary
	if [ ! "$goodformat" == 1 ]
	then
		echo "$f doesn't seem to have SRR's in header ($hd), renaming"
		acc=`echo $f | awk -F/ '{print $2}' | awk -F. '{print $1}'`
		lz4cat $f | sed 's,>N,>'$acc' N,' | lz4 -9 - $f.redone

		filename=$(basename -- "$f")
		extension="${filename##*.}"
		if [ $extension == "lz4" ]
		then
			mv $f.redone $f
			echo "$f written"
		else
			mv $f.redone $f.lz4
			echo "$f.lz4 written, removing old file $f"
			rm -f $f
		fi
	else
		echo "$f is good ($hd)"
	fi

}
export -f job

find contigs/ -name "*.fasta" | parallel -j $cores  "job {}"
find contigs/ -name "*.fa" | parallel -j $cores  "job {}"
find contigs/ -name "*.lz4" | parallel -j $cores  "job {}"

