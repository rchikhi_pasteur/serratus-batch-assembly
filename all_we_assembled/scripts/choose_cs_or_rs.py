import sys,os
import taxoniq


d = {}
for line in open("/home/ec2-user/serratus-batch-assembly/master_table/master_table.csv"):
    #ERR3179774,28214,1,A,checkv,2691591,,NC_009657.1,97.3,hiclip_suspicious,MH687965.1,100.0,near_complete,MG546689.1,85.8,hiclip_suspicious,ILLUMINA
    ls = line.split(',')
    acc = ls[0]
    refseq = ls[7]
    d[acc]=refseq


acc = sys.argv[1]
if acc not in d:
    scientific_name = "unknown"
else:
    refseq = d[acc]
    if '_' not in refseq:
        scientific_name = "unknown"
    else:
        scientific_name = taxoniq.Taxon(taxoniq.Accession(refseq).tax_id).scientific_name

if 'orona' in scientific_name:
    print('coronaspades',f"({scientific_name})")
else:
    print('rnaviralspades',f"({scientific_name})")
