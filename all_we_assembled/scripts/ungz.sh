cores=32

job() {
	f=$1
	echo $f
	decomp=${f%.*}
	
	if [ ! -f $decomp ]
	then
		gunzip $f
	else
		echo "$decomp already exists"
	fi
}
export -f job

find contigs/ -name "*.gz" | parallel -j $cores  "job {}"
