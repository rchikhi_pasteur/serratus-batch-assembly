g=open("all_serratus_assemblies_05032022.only_circles.fasta","w")
import glob
for f in glob.glob('circles/*.fa'):
    srr = f.split('/')[1].split('.')[0]
    for line in open(f.strip()):
        if ">" in line:
            ls = line.split(" ") 
            header = ls[0]+';'+ls[1]
            line = header
        g.write(line)
g.close()
