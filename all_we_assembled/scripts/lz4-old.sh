# this script compresses to lz4 but doesnt rename headers, hence i'm retiring it. new one is lz4.sh and does all that better 

cores=32

job() {
	set -e
	f=$1
	echo $f
	comp=$f.lz4
	
	if [ -f $comp ]
	then
		echo "$comp already exists, deleting"
		rm -f $comp
	fi 	
	lz4 -9 $f $f.lz4
	rm -f $f
}
export -f job

eval find contigs/ -name *.fa | parallel -j $cores  "job {}"
eval find contigs/ -name *.fasta | parallel -j $cores  "job {}"
