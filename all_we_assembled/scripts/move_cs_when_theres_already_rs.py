# move coronaspades assemblies when we already have assembled rnaviralspades
import sys,os

from collections import defaultdict



files_on_lovelywater = defaultdict(list)
files_on_staged = defaultdict(list)

def has_assembly(acc):
    for f in files_on_lovelywater[acc]:
        if "scaffolds.fasta" in f:
            return True
        if "contigs.fa" in f:
            return True
    return False

def has_cs_assembly_staged(acc):
    for f in files_on_staged[acc]:
        if "coronaspades.scaffolds" in f:
            return True
        if "coronaspades.contigs" in f:
            return True

def has_rs_assembly_staged(acc):
    for f in files_on_staged[acc]:
        if "rnaviralspades.scaffolds" in f:
            return True
        if "rnaviralspades.contigs" in f:
            return True

for line in open("all_files_lovelywater.txt"):
    line = line.strip()
    acc = line.split('.')[0]
    files_on_lovelywater[acc] += [line]

os.system("ls -1 staged/ > all_files_staged.txt")
for line in open("all_files_staged.txt"):
    line = line.strip()
    acc = line.split('.')[0]
    if acc in files_on_lovelywater:
        files_on_staged[acc] += [line]


for acc in files_on_staged:
    if not has_assembly(acc):
        #print("!! no assembly on lovelywater")
        pass
    else:
        if acc in files_on_lovelywater or \
                (has_cs_assembly_staged(acc) and has_rs_assembly_staged(acc)):
            print(acc)
            print("on lovelywater:                                                                                         on staged:")
            for i in range(12):
                print("%-100s" % list(files_on_lovelywater[acc])[i] if len(files_on_lovelywater[acc])>i else "%100s" % "",  \
                      list(files_on_staged[acc])[i]      if len(files_on_staged[acc])>i else "")

            # executed once, don't need it anymore now
            #for f in files_on_staged[acc]:
            #    os.system("mv  staged/%s coronaspades_when_already_rnaviralspades/" % f)
