# this script extracts circular contigs from lz4 contigs using Robert's script

cores=32

declare -A kvalues
while IFS= read -r line; do
	srr=$(echo $line | cut -f1 -d" ")
	k=$(echo $line | cut -f2 -d" ")
	kvalues[$srr]=$k
	#echo "$srr $k"
done < all_serratus_assemblies_05032022.k_values.txt

#https://stackoverflow.com/questions/55316466/how-to-pass-associative-array-to-gnu-parallel
declare -x serialized_array=$(mktemp)   
declare -p kvalues > "${serialized_array}" 

# perform cleanup after finishing script                                      
cleanup() {                                                                   
	rm "${serialized_array}"                                                    
}                                                                             
trap cleanup EXIT         

echo "k values populated"
mkdir -p circles

job() {
	set -e
	f=$1

	source "${serialized_array}"

	srr=`echo $f | awk -F/ '{print $2}' | awk -F. '{print $1}'`
	k=${kvalues[$srr]}
	echo "$srr $k"
	
	lz4cat $f | python spades_circular_contigs/spades_circular_contigs.py /dev/stdin $k > circles/$srr.fa
}
export -f job

find contigs/ -name "*.lz4" | parallel -j $cores  "job {}"

