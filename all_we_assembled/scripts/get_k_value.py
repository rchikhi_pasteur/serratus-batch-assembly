# parse a SPAdes log to extract k value
# in some cases, (small filesize), we've already pre-exctracted it from the assembly graph

import sys, os

filename = sys.argv[1]
b = os.path.getsize(filename)
if b < 50:
    # special case:
    ls = open(filename).read().strip().split()
    assert(len(ls) == 2 and "RR" in ls[0])
    print(ls[-1])
    exit(0)

for line in open(filename):
            if line.startswith("K values to be used:") or line.startswith("  k:"):
                if "auto" in line:
                    last_k_value = "auto"
                else:
                    last_k_value = int(line.split()[-1].replace('[','').replace(']',''))
                    #print("found last k values:",line,flush=True)

print(last_k_value)
