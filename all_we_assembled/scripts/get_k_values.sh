SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
rm -f k_values.txt
for file in `find /rs2/contigs/ -type f`
do
	srr=$(basename $file | cut -d. -f1)
	k_value=`bash $SCRIPT_DIR/get_k_value.sh $srr`
	echo $srr $k_value |tee -a k_values.txt
done
