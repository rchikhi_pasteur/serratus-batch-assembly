import os,sys

os.system("find contigs/ -type f > list_contigs.txt")

d = {}
for line in open("list_contigs.txt"):
    line = line.strip("contigs/").strip()
    ls = line.split('.')
    accession = ls[0]
    is_mfc = ls[-1] == "mfc"
    if accession not in d:
        d[accession]=set()
    d[accession].add("uncompressed" if not is_mfc else "mfc")

for accession in d:
    contents = d[accession]
    if len(contents)==1 and "mfc" in contents:
        print(accession,contents)

os.system("rm -f list_contigs.txt")
