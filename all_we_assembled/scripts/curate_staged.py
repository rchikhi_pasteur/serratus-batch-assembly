# inspect the staged folder and print accessions that have no scaffolds

import sys,os
import pandas as pd

os.system("find staged/ -type f > staged.txt")

#df = pd.DataFrame()
d = {}

for line in open("staged.txt"):
    line = line.strip("staged/").strip()
    ls = line.split('.')
    accession = ls[0]
    assembler = ls[1]
    filetype = '.'.join(ls[2:])
    if accession not in d: d[accession]=set()
    d[accession].add(filetype)


for accession in d:
    if "scaffolds.fasta" not in d[accession] and "contigs.fasta" not in d[accession] and "contigs.fa" not in d[accession]:
        print(accession,d[accession])




os.system("rm -f staged.txt")
