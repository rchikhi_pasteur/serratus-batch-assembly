import sys,os

from collections import defaultdict

files_on_lovelywater = defaultdict(list)
files_on_public = defaultdict(list)

for line in open('all_files_lovelywater.andstaged.txt'):
    line = line.strip()
    acc = line.split('.')[0]
    files_on_lovelywater[acc] += [line]

def has_assembly(acc):
    for f in files_on_lovelywater[acc]:
        if "scaffolds.fasta" in f:
            return True
        if "contigs.fa" in f:
            return True
    return False

for line in open("all_files_public_other.txt"):
    line = line.strip()
    acc = line.split('.')[0]
    if acc in files_on_lovelywater:
        files_on_public[acc] += [line]

for acc in files_on_public:
    if not has_assembly(acc):
        for f in files_on_public[acc]:
            if "scaffolds.fasta" in f or "contigs.fa" in f:
                if "--short" in sys.argv:
                    print(".".join(f.split(".")[:2]))
                else:
                    print(f)
