ls -1 ~/serratus-assemblies/ | grep coronaspades.gene_clusters.fa$  | sed 's/.coronaspades.gene_clusters.fa//g' |sort > list_assembled.txt
ls -1 ~/serratus-assemblies/ | grep coronaspades.bgc_statistics.txt$  | sed 's/.coronaspades.bgc_statistics.txt//g' |sort > list_assembled.bgc.txt
diff  list_assembled.txt list_assembled.bgc.txt  |awk '{print $2}' | sort |uniq > list_assembled.non_bgc.txt
