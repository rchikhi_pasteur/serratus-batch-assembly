cd data
echo "warning: lotsa threads"
#aws s3 sync s3://serratus-public/assemblies/other/   . --exclude "*" --include "*.darth.tar.gz" #no! too big!
for accession in `cat ~/serratus-batch-assembly/lists/pyfasta.bug.0728.txt`
do	
	aws s3 cp s3://serratus-public/assemblies/annotations/$accession.fa.darth.alignments.fasta . &
done

wait
