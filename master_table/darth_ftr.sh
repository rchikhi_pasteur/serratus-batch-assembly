extractjob () {
        f=$1
	accession=${f%%.*}
	#tar --extract --file=$f $accession.darth/$accession/$accession.vadr.pass.tbl
	tar --extract --file=$f $accession.darth/$accession/$accession.vadr.ftr
}
export -f extractjob
cd data
ls -1 *.darth.stripped.tar.gz | parallel -j15 "extractjob {}"

