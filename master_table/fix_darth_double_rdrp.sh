fix () {
	accession=$1

	canonicalize_contigs.sh $PWD/../assemblies/$accession.fa $accession.darth /darth > /dev/null
	aws s3 cp $accession.darth/transeq/alignments.fasta s3://serratus-public/assemblies/annotations/$accession.fa.darth.alignments.fasta
	aws s3 cp $accession.darth/transeq/match-alignments.sto s3://serratus-public/assemblies/annotations/$accession.fa.darth.alignments.sto
	echo "uploaded $accession.darth/transeq/alignments.fasta"
	ls -l $accession.darth/transeq/alignments.fasta
	rm -Rf $accession.darth
}
export -f fix
cat ~/serratus-batch-assembly/master_table/master_table.accessions.txt | parallel -j 15 "fix {}"
