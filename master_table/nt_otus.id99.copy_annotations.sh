cd annotations.old-darth
#aws s3 sync s3://serratus-public/seq/cov5/annotations.nt_otus.id99/ . --exclude="*" --include="*.fasta"
cd ..

cd annotations.new-darth
aws s3 sync s3://serratus-public/seq/cov5/annotations.nt_otus.id99.redone-transeq/ . --exclude="*" --include="*.fasta"
cd ..
