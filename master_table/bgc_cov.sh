find data/ -name "*.bgc_statistics.txt" | 
	parallel -j15 \
	"python ~/serratus-batch-assembly/stats/bgc_parse_and_extract.py {}"
