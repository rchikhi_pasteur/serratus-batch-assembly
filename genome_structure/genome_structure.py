import csv, os
from collections import defaultdict

# get single-contig accessions
# also get number of contigs / total length per accession

nb_contigs = dict()
length = dict()
with open("../master_table/master_table.csv") as csvfile:
#with open("master_table.csv") as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        nbc = int(row['nb_contigs'])
        l   = int(row['length'])
        nb_contigs[row['accession']] = nbc
        length[    row['accession']] = l


## Load OTUs/accession correspondance

def strip(x):
    return x.replace('/G','').replace('/A','')

otu99, otu97 = set(), set()
dotu97 = defaultdict(list)
for line in open("../lists/label_otu.id99_97.tsv"):
    if len(line) == 0: continue
    label, ex99, ex97 = line.strip().split()
    label, ex99, ex97 = strip(label), strip(ex99), strip(ex97)
    otu99.add(ex99)
    otu97.add(ex97)
    #print(label,ex99,ex97)
    dotu97[ex97] += [label]

print(len(otu97),"OTUs 97%")
print(len(otu99),"OTUs 99%")

# gather stats for GB genomes too
from pyfaidx import Fasta
from glob import glob
for genome in otu97:
    if genome in length: continue
    try:
        f = Fasta(glob("/home/ec2-user/master_table/nt_otus.id99/split/"+genome+"*.fa")[0])
    except:
        print("can't find " + genome)
        continue
    length[genome]    = sum([len(x) for x in f])
    nb_contigs[genome]= len([x         for x in f])


def nido_transform_structures(genome_structures_list):
    # gather all nido annotations and put them in non-nido with proper flag
    genome_structures = defaultdict(list)
    for structure in genome_structures_list:
        accession = structure[0]
        features = structure[1]
        if "nido" in accession:
            real_accession = accession.split('.')[0]
            pfam_hmm = "nido"
            for feature in features:
                genome_structures[real_accession] += [tuple(list(feature) + [pfam_hmm])]
        else:
            pfam_hmm = "sc2"
            genome_structures[accession] += [tuple(list(feature) + [pfam_hmm]) for feature in features]
    return genome_structures

# Load genome structures

import marshal
marshal_filename = "genome_structure.data.marshal"
if not os.path.exists(marshal_filename) or os.stat(marshal_filename).st_size == 0:
    print("creating marshal file from","genome_structure.data.py","data")
    gsdata    = open("genome_structure.data.py")   .read()
    gsdata_gb = open("genome_structure.data.gb.py").read()
    genome_structures_list = eval("("+gsdata   +")") \
                           + eval("("+gsdata_gb+")")

    genome_structures = nido_transform_structures(genome_structures_list)
    #print(list(genome_structures.items())[:3])

    outmarshal = open(marshal_filename,"wb")
    marshal.dump(list(genome_structures.items()),outmarshal)
    outmarshal.close()
    # also write it to CSV for good measure
    tsv_outfile = open("genome_structure.data.tsv","w")
    tsv_writer = csv.writer(tsv_outfile, delimiter='\t')
    tsv_writer.writerow(['accession','start_position','end_position','strand','gene_name','contig_name','is_complete', 'pfam_hmm'])
    for accession, features in genome_structures.items():
        if len(features) == 0: continue
        for feature in features:
            tsv_writer.writerow([accession] + list(feature))
    tsv_outfile.close()

else:
    # load marshal file (faster)
    print("opening marshal file",marshal_filename)
    gsdata = open(marshal_filename,'rb')
    genome_structures = dict( marshal.load(gsdata) )
    gsdata.close()
print(len(genome_structures),"structures")

dgs     = dict()
dgs_ids = dict()
dgs_ids_complete = dict()
for accession, lst in genome_structures.items():
    dgs[accession]     = lst
    dgs_ids[accession] = set()
    for start,end,strand,domain,ctg,is_complete, pfam_hmm in lst:
        dgs_ids[accession].add(domain)
        if (accession,domain) not in dgs_ids_complete or not dgs_ids_complete[(accession, domain)]:
            dgs_ids_complete[(accession, domain)] = is_complete


# Get list of RdRP-containing, single-contig accessions for each OTU

to_plot_sc_rdrp = set()
to_plot_rdrp = set() # this is the accessions that have multiple contigs, but have rdrp, and taking the longest per OTU

def has_rdrp(accession):
    if accession in dgs_ids and 'RdRP_1' in dgs_ids[accession]:
        if not dgs_ids_complete[(accession,'RdRP_1')]:
            print(accession,"has incomplete RdRP")
            return False
        else:
            return True
    return False

contains_rdrp = set()
for accession in dgs_ids:
    if has_rdrp(accession):
        contains_rdrp.add(accession)
g = open("genome_structure.rdrp_accessions.txt","w")
for accession in contains_rdrp:
    is_complete = dgs_ids_complete[(accession, 'RdRP_1')]
    g.write("%s %s\n" % (accession, str(is_complete)))
g.close()

search_for_good_representative = False
if search_for_good_representative:
    nb_single_contig_found = 0
    nb_single_contig_and_rdrp_found = 0
    nb_rdrp = 0
    for ex97 in sorted(list(otu97)):
        found_single_contig = None
        found_single_contig_rdrp = None
        found_rdrp = []
        for accession in dotu97[ex97]:
            #print(ex97,accession)
            if accession not in nb_contigs: 
                print("accession",accession,"isn't known to us")
                continue
            if nb_contigs[accession] == 1:
                if found_single_contig is None:
                    nb_single_contig_found += 1
                found_single_contig = accession
                if has_rdrp(accession):
                    if found_single_contig_rdrp is None:
                        nb_single_contig_and_rdrp_found += 1
                    found_single_contig_rdrp = accession
                    #break
            if has_rdrp(accession):
                if len(found_rdrp) == 0:
                    nb_rdrp += 1
                found_rdrp += [(length[accession],accession)]
        
        print("OTU",ex97,"(%d accessions)" % len(dotu97[ex97])) 
        if found_single_contig_rdrp is None:
            print("1. couldn't find a single single-contig RdRP-containing accession")
        else:
            print("1. using",found_single_contig,"total length",length[found_single_contig])
            to_plot_sc_rdrp.add((found_single_contig,ex97))
        if len(found_rdrp) > 0:
            l ,accession = sorted([x for x in found_rdrp if x[0] < 39000])[-1] # take the longest assembly that's below 39 kbp
            print("2. longest assembly with RdRP is",accession,"of total length",l)
            to_plot_rdrp.add((accession,ex97))
        else:
            print("2. couldn't find a single RdRP-containing accession (how's this even possible?!)")

    print(nb_single_contig_found,"/",len(otu97),"single-contig assemblies found in OTUs")
    print(nb_rdrp               ,"/",len(otu97),"complete RdRP found in OTUs")
    print(nb_single_contig_and_rdrp_found,"/",len(otu97),"single-contig AND complete RdRP found in OTUs")

    # add Toro and Epsy to plot
    torolike = "SRR4920045 SRR5234495 SRR6291266"
    epsys = """ERR3994223
    SRR1324965
    SRR5997671
    SRR10917299
    SRR8389791
    SRR2418554
    SRR7507741
    SRR6788790
    SRR6311475"""
    for collection_name, collection_lst in [('toro',torolike.split()),('Epsy',epsys.split('\n'))]:
        for elt in collection_lst:
            elt = elt.strip()
            to_plot_rdrp.add((elt,collection_name))
            if nb_contigs[elt] == 1:
                to_plot_sc_rdrp.add((elt,collection_name))
            else:
                print(elt,"exemplar of OTU",collection_name,"has",nb_contigs[elt],"contigs")
            if elt in otu97:
                for accession in dotu97[elt]:
                    to_plot_rdrp.add((elt,collection_name))
                    if nb_contigs[accession] == 1:
                        to_plot_sc_rdrp.add((accession,collection_name))
                        print("added",accession,"for",collection_name)
                    else:
                        print("from OTU",collection_name,",",accession,"has",nb_contigs[elt],"contigs")
else:
    # now we just plot exemplars, easy
    for ex97 in sorted(list(otu97)):
        to_plot_rdrp.add((ex97,ex97))


g = open("genome_structure.to_plot.sc_rdrp.txt","w")
h = open("genome_structure.to_plot.rdrp.txt","w")
for accession, why in to_plot_sc_rdrp:
    g.write("%s %s\n" % (accession, why))
g.close()
for accession, why in to_plot_rdrp:
    h.write("%s %s\n" % (accession, why))
h.close()

g = open("genome_structure.lengths.txt","w")
for accession in length:
    g.write("%s %d\n" % (accession, length[accession]))
g.close()


