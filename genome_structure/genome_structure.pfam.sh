
structurejob () {
	accession=$1
	epsy=$2
	echo $1 $2 
	#feb 2021 addition to do some Epsys stored in different location 
	if [ "$epsy" == "epsy" ]
	then
		assembly=$PWD/$accession
		accession=$(basename $assembly | cut -d. -f1,2)
	else
		assembly=$HOME/master_table/assemblies/$accession.fa
	fi
	ls -l $assembly

	# canonicalize assembly
	srr=$(echo $accession | cut -d. -f1)
	if [[ "$srr" =~ ^(SRR8389847|SRR8389791|nb1-SRR10402291|nb2-SRR6311475)$ ]]; then
		# these ones are regular one-contig assemblies
		echo "$accession canonicalized normally"
		~/serratus-batch-assembly/src/darth/src/canonicalize_contigs.sh $assembly $accession.canonicalize /darth
	else
		echo "$accession canonicalized like an epsy"
		~/serratus-batch-assembly/src/darth/src/canonicalize_contigs.epsy.sh $assembly $accession.canonicalize /darth
	fi
	
	if [ "$epsy" == "epsy" ]
	then
		assembly=$accession.canonical.fa
		mv $accession.canonicalize/transeq/canonical.fna $assembly
	else
		mv $accession.canonicalize/transeq/canonical.fna $HOME/master_table/assemblies.canonical/$accession.fa
		assembly=$HOME/master_table/assemblies.canonical/$accession.fa
	fi
	ls -l $assembly


	# try two methods: transeq and getorf
	for flavor in ".transeq" ".orfs"
	do
		if [[ "$flavor" == ".orfs" ]]
		then
			input=genome_structure/$accession.orfs.fa
			getorf -minsize 300 $assembly $input
		else
			input=genome_structure/$accession.transeq.fa
			transeq -frame 6 $assembly $input
		fi

		# try two hmms
		#for hmm in "sars-cov-2" "nido"
		# feb 2021: just do epsy
		for hmm in "epsy"
		do
			if [ "$hmm" == "sars-cov-2" ]
			then
				hmm="/darth/Pfam-A.SARS-CoV-2.hmm"
				output_folder=$HOME/master_table
			elif [ "$hmm" == "nido" ]
			then
				hmm="$HOME/nido-pfam/Nido-Pfam-A.hmm"
				output_folder=$HOME/nido-pfam
			elif [ "$hmm" == "epsy" ]
			then
				hmm="$HOME/master_table/epsy/Epsy.hmm"
				output_folder=$PWD/
			fi
			sto=$output_folder/genome_structure/$accession"$flavor".sto
			tbl=$output_folder/genome_structure/$accession"$flavor".tbl
			domtbl=$output_folder/genome_structure/$accession"$flavor".domtbl
			hmmout=$output_folder/genome_structure/$accession"$flavor".hmmsearch_stdout
			#hmmsearch --cut_ga # old params; new one for nido below
			hmmsearch --max -E 0.01 -A $sto --tblout $tbl --domtblout $domtbl -o $hmmout $hmm $input
		done
	done

	rm -Rf $accession.canonicalize
}
export -f structurejob
#cat ~/serratus-batch-assembly/master_table/master_table.accessions.txt  | parallel -j 15  "structurejob {}"
mkdir -p genome_structure
#find splitted/ -name "*.fa" |parallel -j 15  "structurejob {} epsy"
#find splitted/ -name "nb1*.fa" |parallel -j 15  "structurejob {} epsy"
find splitted/ -name "nb2*.fa" |parallel -j 15  "structurejob {} epsy"
