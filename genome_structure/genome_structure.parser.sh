rm -f genome_structure.data.py
#for folder in "$HOME/master_table" "$HOME/nido-pfam"
for folder in "$HOME/master_table/epsy"
do
	for domtbl in `ls -1 $folder/genome_structure/*.transeq.domtbl`
	do
		python genome_structure.parser.py $domtbl >> genome_structure.data.py
	done
done
