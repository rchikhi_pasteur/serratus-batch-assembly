
structurejob () {
	accession=$1
	assembly=$(ls -1 $HOME/master_table/nt_otus.id99/split/$accession.*)
	echo "assembly: $assembly"

	# canonicalize assembly
	~/serratus-batch-assembly/src/darth/src/canonicalize_contigs.sh $assembly $accession.canonicalize /darth
	mv $accession.canonicalize/transeq/canonical.fna $HOME/master_table/nt_otus.id99/canonical/$accession.fa
	assembly=$HOME/master_table/nt_otus.id99/canonical/$accession.fa
	
	# try two methods: transeq and getorf
	for flavor in ".transeq" ".orfs"
	do
		output_folder=$HOME/master_table/nt_otus.id99/genome_structure
		if [[ "$flavor" == ".orfs" ]]
		then
			input=$output_folder/$accession.orfs.fa
			getorf -minsize 300 $assembly $input
		else
			input=$output_folder/$accession.transeq.fa
			transeq -frame 6 $assembly $input
		fi

		# try two hmms
		for hmm in "sars-cov-2"
		do
			hmm="/darth/Pfam-A.SARS-CoV-2.hmm"
			sto=$output_folder/$accession"$flavor".sto
			tbl=$output_folder/$accession"$flavor".tbl
			domtbl=$output_folder/$accession"$flavor".domtbl
			hmmout=$output_folder/$accession"$flavor".hmmsearch_stdout
			#hmmsearch --cut_ga # old params; new one for nido below
			hmmsearch --max -E 0.01 -A $sto --tblout $tbl --domtblout $domtbl -o $hmmout $hmm $input
		done
	done

	rm -Rf $accession.canonicalize
}
export -f structurejob
cat ~/serratus-batch-assembly/lists/label_otu.id99_97.tsv | awk '{print $3}' | grep '/G$' | sed 's/\/G//g' | sort | uniq | parallel -j 15  "structurejob {}"
