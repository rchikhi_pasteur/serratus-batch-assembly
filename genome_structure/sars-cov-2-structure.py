[(288, 693, '+', 'bCoV_NSP1', 'NC_045512.2_2', True),
(810, 1533, '+', 'CoV_NSP2_N', 'NC_045512.2_2', True),
(2217, 2718, '+', 'CoV_NSP2_C', 'NC_045512.2_2', True),
(2901, 3414, '+', 'bCoV_NSP3_N', 'NC_045512.2_2', True),
(3435, 3756, '+', 'Macro', 'NC_045512.2_2', True),
(4314, 4743, '+', 'bCoV_SUD_M', 'NC_045512.2_2', True),
(4755, 4947, '+', 'bCoV_SUD_C', 'NC_045512.2_2', True),
(4953, 5910, '+', 'CoV_peptidase', 'NC_045512.2_2', True),
(6027, 6321, '+', 'bCoV_NAR', 'NC_045512.2_2', True),
(7044, 8508, '+', 'CoV_NSP3_C', 'NC_045512.2_2', True),
(8625, 9687, '+', 'CoV_NSP4_N', 'NC_045512.2_2', True),
(9759, 10047, '+', 'CoV_NSP4_C', 'NC_045512.2_2', True),
(10137, 11010, '+', 'Peptidase_C30', 'NC_045512.2_2', True),
(11841, 12090, '+', 'CoV_NSP7', 'NC_045512.2_2', True),
(12090, 12681, '+', 'CoV_NSP8', 'NC_045512.2_2', True),
(12684, 13023, '+', 'CoV_NSP9', 'NC_045512.2_2', True),
(13047, 13416, '+', 'CoV_NSP10', 'NC_045512.2_2', True),
(13482, 14538, '+', 'CoV_RPol_N', 'NC_045512.2_1', True),
(14637, 16104, '+', 'RdRP_1', 'NC_045512.2_1', True),
(17277, 17952, '+', 'Viral_helicase1', 'NC_045512.2_1', True),
(18048, 19614, '+', 'CoV_Methyltr_1', 'NC_045512.2_1', True),
(19620, 19803, '+', 'CoV_NSP15_N', 'NC_045512.2_1', True),
(19830, 20121, '+', 'CoV_NSP15_M', 'NC_045512.2_1', True),
(20193, 20652, '+', 'CoV_NSP15_C', 'NC_045512.2_1', True),
(20661, 21549, '+', 'CoV_Methyltr_2', 'NC_045512.2_1', True),
(21657, 22572, '+', 'bCoV_S1_N', 'NC_045512.2_2', True),
(22605, 23139, '+', 'bCoV_S1_RBD', 'NC_045512.2_2', True),
(23166, 23337, '+', 'CoV_S1_C', 'NC_045512.2_2', True),
(23691, 25257, '+', 'CoV_S2', 'NC_045512.2_2', True),
(25392, 26214, '+', 'bCoV_viroporin', 'NC_045512.2_1', True),
(26250, 26448, '+', 'CoV_E', 'NC_045512.2_1', True),
(26565, 27168, '+', 'CoV_M', 'NC_045512.2_3', True),
(27201, 27384, '+', 'bCoV_NS6', 'NC_045512.2_1', True),
(27438, 27756, '+', 'bCoV_NS7A', 'NC_045512.2_1', True),
(27753, 27879, '+', 'bCoV_NS7B', 'NC_045512.2_3', True),
(27891, 28245, '+', 'bCoV_NS8', 'NC_045512.2_3', True),
(28281, 28572, '+', 'bCoV_lipid_BD', 'NC_045512.2_3', True),
(28395, 29418, '+', 'CoV_nucleocap', 'NC_045512.2_2', True),
(28731, 28941, '+', 'bCoV_Orf14', 'NC_045512.2_3', True)]
