rm -rf OTUs.reduced_rdrps.fa
cat ../lists/OTUs_ordered_accessions.to_plot.named_bold.txt | awk '{print $1}' |xargs -I{} grep -i rdrp ~/master_table/data/{}.fa.darth.alignments.fasta -A1 | grep -v '^--$' >> OTUs.reduced_rdrps.fa
cat ../lists/OTUs_ordered_accessions.to_plot.named_bold.txt | awk '{print $1}' |xargs -I{} grep -ri rdrp --include="{}.*.nt_otus.id99.fa.darth.alignments.fasta" $HOME/master_table/nt_otus.id99/annotations.new-darth/ -A1 | grep -v '^--$' >> OTUs.reduced_rdrps.fa
