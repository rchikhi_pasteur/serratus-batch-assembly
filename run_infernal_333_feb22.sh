nb_threads=`grep RayanSerratusAssemblyHimemBatchJobDefinition: template/template.yaml -A 15|grep Vcpus |awk '{print $2}'`
echo "setting nb_threads=$nb_threads"

#for f in `cat lists/infernal_333_feb22.down_to_59.txt`
#for f in `cat find_non_assembled.txt`
for f in `cat lists/infernal_333_feb22.19_I_thought_I_had.txt`
do
    python simple_submit_job.py $f infernal_59_feb22 $nb_threads 
done

#very last one:
#  python simple_submit_job.py ERR868448 infernal_59_feb22 32
