import json
import boto3
import sys

if len(sys.argv) < 2:
    exit("argument: [accession]")

accession = sys.argv[1]
if "RR" not in accession:
    exit("accession should be of the form: [E/S]RR[0-9]+")

batch = boto3.client('batch')
#region = batch.meta.region_name
region = "us-east-1"

jobDefinition = 'RayanSerratusAssemblyHimemBatchJobDefinition'

response = batch.submit_job(jobName='RayanSerratusAssemblyBatchJobQueue', 
                            jobQueue='RayanSerratusAssemblyBatchJobQueue', 
                            jobDefinition=jobDefinition, 
                            containerOverrides={
                                "command": [ "python", "satellite_batch_processor.py"],
                                "environment": [ 
                                    {"name": "Accession", "value": accession},
                                    {"name": "Region", "value": region},
                                ]
                            })


print("Job ID is {}.".format(response['jobId']))
