# find the k value
# under the assumption that we're only looking at rnaviralspades assemblies

srr=$1
if [ -z "$srr" ]
then
	      echo "$srr is empty"
	      exit 1
fi

log=`grep $srr ../all_we_assembled/all_files_lovelywater.txt |grep rnaviralspades | grep log |awk '{print "s3://lovelywater/assembly/contigs/"$1}' |head -n 1`

if [ -z "$log" ]
then
	echo "couldn't find logfile in lovelywater, retrying on serratus-public" 1>&2;
	log=`grep $srr ../all_we_assembled/all_files_public.txt |grep rnaviralspades | grep log |awk '{print "s3://serratus-public/"$4}' |head -n 1`
fi

if [ -z "$log" ]
then
	echo "couldn't find logfile in public with rnaviral, retrying with coronaspades on public" 1>&2;
	log=`grep $srr ../all_we_assembled/all_files_public.txt |grep coronaspades | grep log |awk '{print "s3://serratus-public/"$4}' |head -n 1`
fi

if [ -z "$log" ]
then
	echo "couldn't find logfile, retrying with coronaspades on lovelywater" 1>&2;
	log=`grep $srr ../all_we_assembled/all_files_lovelywater.txt |grep coronaspades | grep log |awk '{print "s3://lovelywater/assembly/contigs/"$1}' |head -n 1`
fi


if [ -z "$log" ]
then
	echo "can't find logfile" 1>&2
	echo "-1"
	exit 0
else
	echo "found logfile: $log" 1>&2
fi

rm -Rf $srr.log
aws s3 cp $log $srr.log 1>&2
python get_k_value.py $srr.log
rm -Rf $srr.log
