import sys

for line in open(sys.argv[1]):
            if line.startswith("K values to be used:") or line.startswith("  k:"):
                if "auto" in line:
                    last_k_value = "auto"
                else:
                    last_k_value = int(line.split()[-1].replace('[','').replace(']',''))
                    #print("found last k values:",line,flush=True)

print(last_k_value)
