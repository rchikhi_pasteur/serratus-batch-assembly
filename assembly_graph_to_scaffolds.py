# converts a assembly_graph_with_scaffolds.gfa to scaffolds.fasta
# assumes the .gfa file is such that S lines are before L lines which are before P lines. this is typically the case in spades output
import sys
assembly_graph=sys.argv[1]

def revcomp(st):
    nn = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A', 'N': 'N'}
    return "".join(nn[n] for n in reversed(st))

seq_dict=dict()
edges=set()
for line in open(assembly_graph):
    if line[0] == 'S':
        #S       24769   AGGGAGAAGCAGGCTCCCTATGGGGAGCCCAAAGTGGGACTCGATCCCAG      KC:i:104
        ls=line.split()
        line_id, seq=ls[1],ls[2]
        seq_dict[line_id] = seq

    elif line[0] == 'L':
        #L       3       +       20601   -       29M
        ls = line.split()
        a,b = int(ls[1]),int(ls[3])
        if a > b: a,b = b,a
        edges.add((a,b))
        k=int(ls[-1][:-1])

    elif line[0] == 'P':
       #P       NODE_14766_length_84_cov_3.036364_1     23309+,21015-   * 
       ls = line.split()
       node, path = ls[1],ls[2]
       print(">"+node)
       s = ""
       prev=None
       for line_id in path.split(','):
           o = line_id[-1]
           line_id = line_id[:-1]
           seq = seq_dict[line_id]
           if o == '-':
               seq = revcomp(seq)
           if s != "":
               a,b = prev, int(line_id)
               if a > b: a,b = b,a
               if (a,b) in edges:
                   seq = seq[k:]
               else:
                   seq = "N"+seq
                   sys.stderr.write(f"[warning, minor:] found elusive missing edge in scaffold path: {node} {(a,b)}")
           s += seq
           prev = int(line_id)
       print(s)

