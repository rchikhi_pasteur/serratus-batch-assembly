import boto3
from boto3.dynamodb.conditions import Key, Attr
import csv, sys, argparse
from datetime import datetime
import json
import os
import urllib3
import glob

import darth
import serra
import reads
import coronaspades

LOGTYPE_ERROR = 'ERROR'
LOGTYPE_INFO = 'INFO'
LOGTYPE_DEBUG = 'DEBUG'

# other parameters
nb_threads = str(8)

def process_file(accession, region):

    urllib3.disable_warnings()
    s3 = boto3.client('s3')
    sdb = boto3.client('sdb', region_name=region)
    inputBucket = "serratus-rayan"
    outputBucket = "serratus-public"
    assembler = "rnaviralspades-sat"
    s3_folder = "satellite/other/" + accession + "." + assembler + "/"
    s3_assembly_folder = "satellite/contigs/"        
    inputDataFn = accession+".inputdata.txt"

    print("region - " + region, flush=True)
    print("accession - " + accession, flush=True)
    # check free space
    os.system(' '.join(["df", "-h", "."]))

    startBatchTime = datetime.now()
    
    os.chdir("/serratus-data")

    force_redownload=False
    reads.get_reads(accession, s3, s3_folder, outputBucket, force_redownload, sdb, nb_threads, inputDataFn)
    
    local_file = accession + ".fastq"
    
    output_contigs, assembly_already_made = coronaspades.coronaspades(accession, inputDataFn, local_file, "osef", outputBucket, s3_folder, s3_assembly_folder, s3, sdb, nb_threads, None, "rnaviralspades")

    # finishing up
    endBatchTime = datetime.now()
    diffTime = endBatchTime - startBatchTime
    print(accession, "File processing time - " + str(diffTime.seconds), flush=True) 


def main():
    accession = ""
    region = "us-east-1"
   
    if "Accession" in os.environ:
        accession = os.environ.get("Accession")
    if "Region" in os.environ:
        region = os.environ.get("Region")

    if len(accession) == 0:
        exit("This script needs an environment variable Accession set to something")

    print("accession:",accession, "region: " + region, flush=True)

    try:
        process_file(accession, region)
    except Exception as ex:
        print("Exception occurred during process_file() with arguments", accession, region,flush=True) 
        print(ex,flush=True)
        import traceback
        traceback.print_exc()

    #cleanup
    # it is important that this code isn't in process_file() as that function may stop for any reason
    print("checking free space",flush=True)
    os.chdir("/serratus-data")
    os.system(' '.join(["ls","-Rl",accession+"*"])) 
    os.system(' '.join(["rm","-Rf",accession+"*"])) 
    os.system(' '.join(["df", "-h", "."]))

if __name__ == '__main__':
   main()
