# doesn't run checkv, serratax, darth, etc. It just performs the assembly and uploads it

import boto3
from boto3.dynamodb.conditions import Key, Attr
import csv, sys, argparse
from datetime import datetime
import json
import os
import urllib3
import glob

import darth
import serra
import reads
import coronaspades

LOGTYPE_ERROR = 'ERROR'
LOGTYPE_INFO = 'INFO'
LOGTYPE_DEBUG = 'DEBUG'


def process_file(accession, region, tag, nb_threads):

    urllib3.disable_warnings()
    s3 = boto3.client('s3')
    sdb = boto3.client('sdb', region_name=region)
    inputBucket = "serratus-rayan"
    outputBucket = "serratus-public"
    assembler = "rnaviralspades-" + tag
    #assembler = "metaspades-" + tag
    #assembler = "coronaspades"
    s3_folder = "assemblies/" + tag + "/other/" + accession + "." + assembler + "/"
    s3_assembly_folder = "assemblies/"+ tag + "/contigs/"        
    inputDataFn = accession+".inputdata.txt"

    print("region - " + region, flush=True)
    print("accession - " + accession, flush=True)
    # check free space
    os.system(' '.join(["df", "-h", "."]))

    startBatchTime = datetime.now()
    
    os.chdir("/serratus-data")

    force_redownload=False
    reads.get_reads(accession, s3, s3_folder, outputBucket, force_redownload, sdb, nb_threads, inputDataFn)
    
    local_file = accession + ".fastq"
    
    output_contigs, assembly_already_made = coronaspades.coronaspades(accession, inputDataFn, local_file, "osef", outputBucket, s3_folder, s3_assembly_folder, s3, sdb, nb_threads, None, assembler)

    # finishing up
    endBatchTime = datetime.now()
    diffTime = endBatchTime - startBatchTime
    print(accession, "File processing time - " + str(diffTime.seconds), flush=True) 


def main():
    accession = ""
    region = "us-east-1"
    tag = "unspecified_tag"
    nb_threads = str(8)
   
    if "Accession" in os.environ:
        accession = os.environ.get("Accession")
    if "Region" in os.environ:
        region = os.environ.get("Region")
    if "Tag" in os.environ:
        tag = os.environ.get("Tag")
    if "Threads" in os.environ:
        nb_threads = os.environ.get("Threads")

    if len(accession) == 0:
        exit("This script needs an environment variable Accession set to something")

    print("accession:",accession, "region: " + region, "threads:" + nb_threads, flush=True)

    try:
        process_file(accession, region, tag, nb_threads)
    except Exception as ex:
        print("Exception occurred during process_file() with arguments", accession, region,flush=True) 
        print(ex,flush=True)
        import traceback
        traceback.print_exc()
        sys.stdout.flush()

    #cleanup
    # it is important that this code isn't in process_file() as that function may stop for any reason
    print("checking free space",flush=True)
    os.chdir("/serratus-data")
    os.system(' '.join(["ls","-Rl",accession+"*"])) 
    os.system(' '.join(["rm","-Rf",accession+"*"])) 
    os.system(' '.join(["df", "-h", "."]))
    os.system(' '.join(["df", "-h", "/"]))

if __name__ == '__main__':
   main()
