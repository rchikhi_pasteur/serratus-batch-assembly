from datetime import datetime
import utils
import os
import re

def download_coronaspades_assembly(accession,assembler,outputBucket,s3_folder,s3_assembly_folder, s3, sdb):
    # grab assemblies from S3
    checkv_filtered_contigs = accession + "." + assembler + ".gene_clusters.checkv_filtered.fa" 
    serratax_contigs_input = checkv_filtered_contigs
    try:
        s3.download_file(outputBucket, s3_assembly_folder + serratax_contigs_input, serratax_contigs_input)
        gene_clusters_file = accession + "." + assembler + ".gene_clusters.fa"
        s3.download_file(outputBucket, s3_folder + gene_clusters_file, gene_clusters_file)
    except:
        print("couldn't download coronaspades assembly from S3, stopping",flush=True)
        exit(1)
    return checkv_filtered_contigs, gene_clusters_file


def coronaspades(accession, inputDataFn, local_file, assembler, outputBucket, s3_folder, s3_assembly_folder, s3, sdb, nb_threads, checkv=None, orig_binary_name="coronaspades"):
    version = "SPAdes-3.15.0-Linux"

    binary_name = orig_binary_name
    if len(binary_name.split("-")) > 1:
        binary_name = binary_name.split("-")[0] 
    statsFn = accession + "."+binary_name+".txt"
    contigs_filename = accession+ "."+binary_name+".scaffolds.fa"
   
    # determine if paired-end input
    s3.download_file(outputBucket, s3_folder + inputDataFn, inputDataFn)
    paired_end = False
    with open(inputDataFn) as f:
        for line in f:
            if line.startswith("fastq-dump library type:"):
                paired_end = "paired" in line
 
    # first, determine if we need to run coronaspades _at all_. maybe it was already assembled (or failed to assemble) with latest eersion
    has_logfile = utils.s3_file_exists(s3, outputBucket, s3_folder + statsFn)
    exit_reason = None
    last_k_value = None
    older_version = False
    assembly_already_made = False
    last_input_type = None
    spades_version = ""
    if has_logfile:
        s3.download_file(outputBucket, s3_folder + statsFn, statsFn)
        coronaspades_log = open(statsFn).readlines()
        for line in coronaspades_log:
            if "Cannot allocate memory" in line:
                print("log memory error:",line,flush=True)
                exit_reason = "memory error"
                break
            if "Thank you for using SPAdes!" in line:
                exit_reason = "thankyou"
            if "Domain graph construction constructed, total vertices: 0, edges: 0" in line:
                exit_reason = "nodomain"
                break
            if "SPAdes version:" in line:
                spades_version = line.strip()
            if line.startswith("Command line:"):
                print("log command line:",line,flush=True)
                if re.search(r'\s-s\s',line):
                    last_input_type = "single"
                elif re.search(r'\s--12\s',line):
                    last_input_type = "paired"
                if version not in line:
                    older_version = True
                    # break # don't break until we get the last k value
            if line.startswith("K values to be used:") or line.startswith("  k:"):
                if "auto" in line:
                    last_k_value = "auto"
                else:
                    last_k_value = int(line.split()[-1].replace('[','').replace(']',''))
                    print("found last k values:",line,flush=True)

        print("spades log available, most notable finishing message:",exit_reason,flush=True)

    same_input_type = ((last_input_type == "single" and (not paired_end)) or (last_input_type == "paired" and paired_end))
    if not same_input_type:
        print("previous version was run with a different input type (%s) as the current input type (%s)" % (last_input_type, "paired" if paired_end else "single"),flush=True)

    if exit_reason is not None and older_version == False and same_input_type:
        print("spades was already run,",spades_version,flush=True)
        print("downloading raw and checkv_filtered gene_clusters.fa",flush=True)
        checkv_filtered_contigs, gene_clusters_file = download_coronaspades_assembly(accession,assembler,outputBucket,s3_folder,s3_assembly_folder, s3, sdb)
        os.system('ls -l ' + accession + '*.fa')
        assembly_already_made = True
        return checkv_filtered_contigs, assembly_already_made

    input_type = "--12" if paired_end else "-s"

    k_values = "auto"
    extra_args = []

    #if older_version or (not same_input_type): # was a hack to re-run scaffolding on unpaired-assembled data
    if older_version:
        # special treatment for an older version: get asm graph and rerun from last k value, as per Anton instructions
        if older_version:
            print("older run detected, with",spades_version,flush=True)
        elif (not same_input_type) and (not older_version):
            print("older run detected but same version with not the same input type")
        else:
            print("older run detected")
        print("rerunning with old assembly graph",flush=True)
        if last_k_value is not None:
            k_values = str(last_k_value)
        assembly_graph = accession + ".coronaspades.assembly_graph_with_scaffolds.gfa"
        s3.download_file(outputBucket, s3_folder + assembly_graph + ".gz",  assembly_graph + ".gz" )
        os.system("gunzip " + assembly_graph + ".gz")
        
        # corner case: need to make sure GFA file has a L line
        has_L = False
        with open(assembly_graph) as assembly_graph_file:
            for line in assembly_graph_file:
                if line.startswith('L'):
                    has_L = True
                    break

        if has_L:
            extra_args = ['--assembly-graph',assembly_graph]

    start_time = datetime.now()
    if orig_binary_name == "coronaspades":
        os.system(' '.join(["/%s/bin/%s.py" % (version,binary_name), input_type, local_file,"-k",k_values,"-t",nb_threads,"-o",accession+"_coronaspades"] + extra_args))
    elif orig_binary_name == "metaspades-phage_april21":
        os.system(' '.join(["/%s/bin/%s.py" % (version,binary_name), input_type, local_file,"-k",k_values,"-t",nb_threads,"-o",accession+"_metaspades","--only-assembler"] + extra_args))
    elif orig_binary_name == "rnaviralspades-sat":
        os.system(' '.join(["/%s/bin/%s.py" % (version,binary_name), input_type, local_file,"-k",k_values,"-t",nb_threads,"-o",accession+"_rnaviralspades","--custom-hmms","/satellite/hmm/"] + extra_args))
    elif orig_binary_name == "rnaviralspades-quenya":
        os.system(' '.join(["/%s/bin/%s.py" % (version,binary_name), input_type, local_file,"-k",k_values,"-t",nb_threads,"-o",accession+"_rnaviralspades","--custom-hmms","/aa/quenya.protref.aa"] + extra_args))
    elif orig_binary_name == "rnaviralspades-dicistro":
        os.system(' '.join(["/%s/bin/%s.py" % (version,binary_name), input_type, local_file,"-k",k_values,"-t",nb_threads,"-o",accession+"_rnaviralspades","--custom-hmms","/aa/dicistro.protref.aa"] + extra_args))
    elif binary_name == "rnaviralspades":
        print("using generic rnaviralspades for tag",orig_binary_name,flush=True)
        os.system(' '.join(["/%s/bin/%s.py" % (version,binary_name), input_type, local_file,"-k",k_values,"-t",nb_threads,"-o",accession+"_rnaviralspades","-m","340"] + extra_args))
    else:
        exit("unknown binary " + binary_name)

    coronaspades_time = datetime.now() - start_time
    utils.sdb_log(sdb,accession,binary_name + '_time',coronaspades_time.seconds)

    os.system(' '.join(['cp',accession+"_"+binary_name+"/spades.log",statsFn]))
    s3.upload_file(statsFn, outputBucket, s3_folder + accession + "."+binary_name+".log", ExtraArgs={'ACL': 'public-read'})

    scaffolds_file = accession+ "_"+binary_name+"/scaffolds.fasta"
    os.system(' '.join(['cp',scaffolds_file,contigs_filename]))
    s3.upload_file(scaffolds_file, outputBucket, s3_folder + accession + "."+binary_name+".scaffolds.fasta", ExtraArgs={'ACL': 'public-read'})
      
    paths_file = accession+ "_"+binary_name+"/scaffolds.paths"
    s3.upload_file(paths_file, outputBucket, s3_folder + accession + "."+binary_name+".scaffolds.paths", ExtraArgs={'ACL': 'public-read'})

    os.system('gzip -f ' +  accession + "_"+binary_name+"/assembly_graph_with_scaffolds.gfa")
    assembly_graph_with_scaffolds_filename = accession+ "_"+binary_name+"/assembly_graph_with_scaffolds.gfa.gz"
    s3.upload_file(assembly_graph_with_scaffolds_filename, outputBucket, s3_folder + accession + "."+binary_name+".assembly_graph_with_scaffolds.gfa.gz", ExtraArgs={'ACL': 'public-read'})

    gene_clusters_filename = accession+ "_"+binary_name+"/gene_clusters.fasta"
    if os.path.exists(gene_clusters_filename):
        s3.upload_file(gene_clusters_filename, outputBucket, s3_folder + accession + "."+binary_name+".gene_clusters.fa", ExtraArgs={'ACL': 'public-read'})
    
    domain_graph_filename = accession+ "_"+binary_name+"/domain_graph.dot"
    if os.path.exists(domain_graph_filename):
        s3.upload_file(domain_graph_filename, outputBucket, s3_folder + accession + "."+binary_name+".domain_graph.dot", ExtraArgs={'ACL': 'public-read'})
    
    bgc_statistics_filename = accession+ "_"+binary_name+"/bgc_statistics.txt"
    if os.path.exists(bgc_statistics_filename):
        s3.upload_file(bgc_statistics_filename, outputBucket, s3_folder + accession + "."+binary_name+".bgc_statistics.txt", ExtraArgs={'ACL': 'public-read'})
 
    # run CheckV on gene_clusters
    if checkv is not None:
        checkv_filtered_contigs = checkv(gene_clusters_filename, accession, assembler, outputBucket, s3_folder, s3_assembly_folder, s3, sdb, ".gene_clusters")
        output_filename = checkv_filtered_contigs
    else:
        output_filename = gene_clusters_filename
    return output_filename, assembly_already_made


