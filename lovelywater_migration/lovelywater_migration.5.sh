# https://github.com/ababaian/serratus/issues/237

for org in 1krandom
do
	echo $org
	aws s3 ls s3://serratus-public/assemblies/$org/other/ |grep rnaviralspades > lovelywater_migration.5.txt

	cat lovelywater_migration.5.txt | while read dir 
	do
		dir=$(echo $dir | cut -d " " -f 2)
		echo $dir
		aws s3 cp s3://serratus-public/assemblies/$org/other/$dir s3://serratus-rayan/lovelywater/assembly/contigs/ --exclude "*.inputdata.txt" --recursive --acl "public-read"
	done
done
