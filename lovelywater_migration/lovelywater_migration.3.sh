# https://github.com/ababaian/serratus/issues/237

aws s3 ls s3://serratus-public/satellite/other/ |grep rnaviralspades > lovelywater_migration.3.txt

# satellite rnaviralspades assemblies
cat lovelywater_migration.3.txt | while read dir 
do
	dir=$(echo $dir | cut -d " " -f 2)
	echo $dir
	aws s3 cp s3://serratus-public/satellite/other/$dir s3://serratus-rayan/lovelywater/assembly/contigs/ --exclude "*.inputdata.txt" --recursive --acl "public-read"
done
