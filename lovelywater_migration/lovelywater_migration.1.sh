# https://github.com/ababaian/serratus/issues/237

# assemblies
for assembly in `ls ~/master_table/assemblies/*.fa`
do
	cp $assembly tmp.fa
	#bgzip tmp.fa
	filename=$(basename "$assembly" | cut -f 1 -d '.').cov.fa
	echo $filename
	mv tmp.fa $filename
	aws s3 cp $filename s3://serratus-rayan/lovelywater/assembly/cov/ --acl "public-read"
	rm -f $filename
done

