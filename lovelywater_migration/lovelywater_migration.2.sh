# https://github.com/ababaian/serratus/issues/237


#aws s3 ls s3://serratus-public/assemblies/other/ |grep coronaspades > lovelywater_migration.2.txt

# CS assemblies
cat lovelywater_migration.2.txt | while read dir 
do
	dir=$(echo $dir | cut -d " " -f 2)
	echo $dir
	aws s3 cp s3://serratus-public/assemblies/other/$dir s3://serratus-rayan/lovelywater/contigs/ --exclude "*.inputdata.txt" --recursive --acl "public-read"
done


# annotations
aws s3 cp s3://serratus-public/assemblies/annotations/ s3://serratus-rayan/lovelywater/annotations  --recursive --acl "public-read"
