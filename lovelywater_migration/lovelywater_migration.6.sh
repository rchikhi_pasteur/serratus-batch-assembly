# https://github.com/ababaian/serratus/issues/237

# cleanup of darth inside contigs/ folder

aws s3 ls s3://serratus-rayan/lovelywater/assembly/contigs/ > lovelywater_migration.outcome.contigs.txt

cat lovelywater_migration.outcome.contigs.txt | awk '{print $4}' |grep darth > lovelywater_migration.6.txt

#cat lovelywater_migration.6.txt | parallel -j 8 aws s3 rm s3://serratus-rayan/lovelywater/assembly/contigs/{}

# move of serratax stuff

cat lovelywater_migration.outcome.contigs.txt | awk '{print $4}' |grep serra > lovelywater_migration.6.txt

#cat lovelywater_migration.6.txt | parallel -j 8 aws s3 mv s3://serratus-rayan/lovelywater/assembly/contigs/{} s3://serratus-rayan/lovelywater/assembly/annotation/

# move of checkv stuff

cat lovelywater_migration.outcome.contigs.txt | awk '{print $4}' |grep checkv > lovelywater_migration.6.txt

cat lovelywater_migration.6.txt | parallel -j 16 aws s3 mv s3://serratus-rayan/lovelywater/assembly/contigs/{} s3://serratus-rayan/lovelywater/assembly/annotation/
