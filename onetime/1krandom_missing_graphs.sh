aws s3 ls s3://serratus-public/assemblies/1krandom/other/ --recursive|grep assembly|awk '{print $4}' |cut -d"/" -f4|cut -d. -f1|sort|uniq > 1krandom_missing_graphs.graphs.txt
aws s3 ls s3://serratus-public/assemblies/1krandom/other/ --recursive|grep scaffolds|awk '{print $4}' |cut -d"/" -f4|cut -d. -f1|sort|uniq > 1krandom_missing_graphs.scaffolds.txt
comm -3 1krandom_missing_graphs.scaffolds.txt 1krandom_missing_graphs.graphs.txt > 1krandom_missing_graphs.missing.txt
