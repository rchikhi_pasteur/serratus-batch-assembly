org=$(if [[ $PWD == *"quenya"* ]]; then echo "quenya"; else echo "dicistro"; fi)
echo "in $org mode"

aws s3 sync assemblies/ s3://serratus-public/assemblies/$org/gene_clusters/
