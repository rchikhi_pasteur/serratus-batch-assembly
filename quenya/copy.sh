org=$(if [[ $PWD == *"quenya"* ]]; then echo "quenya"; else echo "dicistro"; fi)
echo "in $org mode"

cd data 

aws s3 sync s3://serratus-public/assemblies/$org/other/   . --exclude "*" --include "*.gene_clusters.fa"
aws s3 sync s3://serratus-public/assemblies/$org/other/   . --exclude "*" --include "*.bgc_statistics.txt"
#echo "warning: takes 600 GB space:"
#aws s3 sync s3://serratus-public/assemblies/$org/other/   . --exclude "*" --include "*.scaffolds.fasta"
#aws s3 sync s3://serratus-public/assemblies/$org/other/   . --exclude "*" --include "*.scaffolds.paths"

cd ..

