org=$(if [[ $PWD == *"quenya"* ]]; then echo "quenya"; else echo "dicistro"; fi)
echo "in $org mode"

for f in `ls all_assembly_graphs.*`
do
	if [ "$org" == "quenya" ]
	then
		aws s3 cp $f s3://serratus-public/assemblies/$org/rdrps_analysis/pathracer/
	else
		aws s3 cp $f s3://serratus-public/assemblies/$org/analysis/
	fi

done
