import gzip
import re
from pyfaidx import Fasta
import os
from multiprocessing import Pool, Lock
from collections import defaultdict

inpt_file = "all_assembly_graphs.RdRP_1234q.scaffold_seqs.centroids.fa.gz"
outp_file = "all_assembly_graphs.RdRP_1234q.scaffold_seqs.centroids.scaffold_nucls.fa"

to_analyze = set()
edges_in_srr = defaultdict(list)
for line in gzip.open(inpt_file):
    line = line.decode()
    if not line.startswith(">"): continue
    edge_id = re.search(r"\|Edges=([\d_']+)\|",line)
    if edge_id is None:
        print("couldn't find Edge= in",line.strip())
    else:
        edge_id = edge_id.group(1)
        edge_id = edge_id.replace('\'','')
        edge_id = edge_id.replace('_',',')
        #print("found Edge=",edge_id)
    srr_id = line[1:].split('.')[0]
    #if ',' not in edge_id: continue # enable to look at only the difficult cases

    #if srr_id != "SRR7153023": continue # to isolate problem

    edges_in_srr[srr_id] += [(line,edge_id)]
    to_analyze.add(srr_id)

    #if len(to_analyze) > 9: break

def analyze(srr_id):
    res = []
    scaffolds_fasta = f"/home/ec2-user/dicistro/data/{srr_id}.rnaviralspades-dicistro/{srr_id}.rnaviralspades.scaffolds.fasta.gz"
    os.system(f"rm -f {scaffolds_fasta}.*i")# && samtools faidx {scaffolds_fasta}") # pyfaidx and samtools faidx don't seem to mix well
    try:
        s = Fasta(scaffolds_fasta)
    except:
        print("somehow can't parse",scaffolds_fasta)
        return res

    for line, edge_id in edges_in_srr[srr_id]:
        scaffolds_paths = open(f"/home/ec2-user/dicistro/data/{srr_id}.rnaviralspades-dicistro/{srr_id}.rnaviralspades.scaffolds.paths")
        node_id = None
        for paths_line in scaffolds_paths:
            paths_line = paths_line.replace('+','').replace('-','').strip()
            if 'NODE' not in paths_line:
                path = set(paths_line.split(','))
                if set(edge_id.split(',')).issubset(path):
                    print("found NODE:",node_id,"for",srr_id,"edge",edge_id,"on path line",paths_line)
                    break
            else:
                node_id = paths_line
        if node_id is None:
            print("couldn't fine NODE for",srr_id)
            res += [("", "", "")]
            continue

        try:
            scaffold = s[node_id]
        except:
            print("couldn't find NODE",node_id,"in",scaffolds_fasta)
            res += [("", "", "")]
            continue

        res += [(line, node_id, str(scaffold))]
    return res

missed = 0
g = open(outp_file,"w")
with Pool(30) as p:
    for lres in p.map(analyze, to_analyze):
        for line, node_id, scaffold in lres: 
            if len(line) == 0: 
                missed += 1
                continue
            g.write(">"+node_id+"."+line[1:].strip()+"\n"+scaffold+"\n")

g.close()
print("done, missed",missed,"entries")
