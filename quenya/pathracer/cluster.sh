#prefix=all_gene_clusters.pathracer_vs_RdRP_1
#cat split/*pathracer*/*.seqs.fa > $prefix.seqs.fa

prefix=all_gene_clusters.pathracer_vs_RdRP_1234q
cat split/*pathracer*/*.seqs.fa > $prefix.seqs.fa
cat all_gene_clusters.pathracer_vs_RdRP_*/*.seqs.fa >> $prefix.seqs.fa

usearch -cluster_fast $prefix.seqs.fa -id 0.97 -centroids $prefix.seqs.centroids.fa -uc $prefix.seqs.uc

diamond makedb --in rdrp0_r1.fa  --db rdrp0_r1
\time diamond blastp --db rdrp0_r1 --query $prefix.seqs.centroids.fa -p 16 --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen qseq sseq > $prefix.seqs.centroids.diamond_vs_rdrp0_r1.fmt6

diamond makedb --in dicistro.protref.aa  --db dicistro.protref
\time diamond blastp --db dicistro.protref --query $prefix.seqs.centroids.fa -p 16 --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen qseq sseq > $prefix.seqs.centroids.diamond_vs_dicistro.protref.fmt6


aws s3 cp $prefix.seqs.centroids.diamond_vs_rdrp0_r1.fmt6 s3://serratus-public/assemblies/dicistro/analysis/
