org=$(if [[ $PWD == *"quenya"* ]]; then echo "quenya"; else echo "dicistro"; fi)
echo "in $org mode"

for x in 1 2 3 4 q
do
	if [ "$x" == "q" ] 
	then
		y=quenya
	else
		y=$x
	fi
	aws s3 cp all_gene_clusters.pathracer_vs_RdRP_$x/RdRP_$y.seqs.fa s3://serratus-public/assemblies/$org/pathracer_seq_fs/
done
