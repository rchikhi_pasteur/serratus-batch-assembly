#what=edges
what=seqs
outfile=all_assembly_graphs.RdRP_1234q.$what.fa
rm -f $outfile
for file in `find *RR*pathracer*| grep $what.fa`
do
	dir=$(dirname $file)
	echo $dir
	accession="${dir%%.*}"
	cp $file tmp
	sed -i "s/>/>$accession./g" tmp
	cat tmp >> $outfile
done


prefix=all_assembly_graphs.RdRP_1234q
usearch -cluster_fast $prefix.seqs.fa -id 0.97 -centroids $prefix.seqs.centroids.fa -uc $prefix.seqs.uc

diamond makedb --in rdrp0_r1.fa  --db rdrp0_r1
\time diamond blastp --db rdrp0_r1 --query $prefix.seqs.centroids.fa -p 16 --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen qseq sseq > $prefix.seqs.centroids.diamond_vs_rdrp0_r1.fmt6

diamond makedb --in dicistro.protref.aa  --db dicistro.protref
\time diamond blastp --db dicistro.protref --query $prefix.seqs.centroids.fa -p 16 --outfmt 6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore qlen slen qseq sseq > $prefix.seqs.centroids.diamond_vs_dicistro.protref.fmt6

pigz -p 48 $outfile
aws s3 cp  $outfile.gz 						      s3://serratus-public/assemblies/dicistro/analysis/
pigz -p 48 $prefix.seqs.centroids.fa
aws s3 cp  $prefix.seqs.centroids.fa.gz 			      s3://serratus-public/assemblies/dicistro/analysis/
pigz -p 48 $prefix.seqs.centroids.diamond_vs_dicistro.protref.fmt6
aws s3 cp  $prefix.seqs.centroids.diamond_vs_dicistro.protref.fmt6.gz s3://serratus-public/assemblies/dicistro/analysis/
pigz -p 48 $prefix.seqs.centroids.diamond_vs_rdrp0_r1.fmt6
aws s3 cp  $prefix.seqs.centroids.diamond_vs_rdrp0_r1.fmt6.gz 	      s3://serratus-public/assemblies/dicistro/analysis/
