find | grep assembly_graph > list_graphs.txt
grep  --with-filename "Thanks for flying us" *RR*.pathracer/*.log | awk -F. '{print $1}' > list_completed.txt

prjob () {
       rdrp=RdRP_1234q
       file=$1
       base=$(basename $file)
       dataset="${base%%.*}"
       if grep -q $dataset list_completed.txt;
       then
	   echo "already done: $base"
	   return
       else  
	       echo $base
       fi

       /home/ec2-user/pathracer/build_spades/bin/pathracer $rdrp.hmm $file --output $dataset.pathracer -t 1  >/dev/null
       rm $dataset.pathracer/*graph_with_hmm_paths.gfa

}
export -f prjob
cat list_graphs.txt | parallel -j 96  "prjob {}"

