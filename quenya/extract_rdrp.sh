file=$1
dataset="${file%%.*}"
makeblastdb -dbtype nucl -in $file
#org=$(if [[ $PWD == *"quenya"* ]]; then echo "quenya"; else echo "dicistro"; fi)
#echo "in $org mode"
# for debugging
#tblastn -db $file -query quenya.protref.aa  -outfmt 6 |sort -n -k 4 | grep rdrp | awk '$4 > 650' | sort -n -k 4 | column -t 
#tblastn -db $file -query $org.protref.aa  -outfmt 6 |sort -n -k 4 | grep rdrp | awk '$4 > 100' | sort -n -k 4 | awk '{print $2"\t"$9"\t"$10}' | awk '$2>$3{x=$3;$3=$2;$2=x}1' | sed 's/ /\t/g' > $file.bed
# new rdrp0 
tblastn -db $file -query rdrp0.fa  -outfmt 6 |sort -n -k 4 | grep rdrp | awk '$4 > 100' | sort -n -k 4 | awk '{print $2"\t"$9"\t"$10}' | awk '$2>$3{x=$3;$3=$2;$2=x}1' | sed 's/ /\t/g' > $file.bed
bedtools sort -i $file.bed > $file.bed.sorted
bed=$file.bed.merged
bedtools merge -i $file.bed.sorted > $bed
rdrp=$dataset.rdrps.fa
bedtools getfasta -fi $file -bed $bed > $rdrp
