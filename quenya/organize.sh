for folder in `ls data/*RR*rnavi* -d`
do
	asm=`ls -1 $folder/*.gene_clusters.fa`
	ls -l $asm
	cp $asm assemblies/
	
	filename=$(basename $asm)
	accession="${filename%%.*}"
	sed -i "s/>/>$accession./g" assemblies/$filename
done
