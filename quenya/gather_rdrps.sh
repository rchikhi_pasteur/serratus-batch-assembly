
rdrpjob () {
	folder=$1
        asm=`ls -1 $folder/*.gene_clusters.fa`
	bash ./extract_rdrp.sh $asm
	filename=$(basename $asm)
	accession="${filename%%.*}"
	rdrpfile=data/$accession.rdrps.fa
	if [ -s "$rdrpfile" ] 
	then
		echo "renaming $rdrpfile"
		sed -i "s/>/>$accession./g" $rdrpfile
	else
		rm $rdrpfile
	fi

}
export -f  rdrpjob
ls data/*RR*rnavi* -d  | parallel -j 48  "rdrpjob {}"
