#time cat ~/serratus-batch-assembly/master_table/master_table.csv                                        |python sra_metadata.py > sra/master_table_11120_SraRunInfo.csv
#time cat ~/serratus-batch-assembly/lists/union_p_sra_score20_CoV_score20_read10.sra_STAT_accessions.txt |python sra_metadata.py > sra/all_assemblies_55715_SraRunInfo.csv
time cat ~/serratus-batch-assembly/master_table/list_latest_ver.txt |python sra_metadata.py > sra/list_latest_ver_SraRunInfo.csv
awk '{sum += $2;} END {print sum/1024/1024/1024/1024}' sra/list_latest_ver_SraRunInfo.csv
echo "(in terabases)"
