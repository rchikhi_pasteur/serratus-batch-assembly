# Serratus SRA Seach Queries

These are the SRA search queries used to generate the `sraRunInfo.csv` files which have been completed. This is a de-facto manifest of the search space. 

```
s3://lovelywater/sra/
├ hu_SraRunInfo.csv.gz      # human (pulic)
├ hu_meta_SraRunInfo.csv.gz # human meta-genomic (public)
├ mu_SraRunInfo.csv.gz      # mouse
├ mamm_SraRunInfo.csv.gz    # mammalian
├ vert_SraRunInfo.csv.gz    # vertebrate
├ viro_SraRunInfo.csv.gz    # virome
└ sra.md5                   # md5sum for SraRunInfo.csv files 
```

### Human (public)

- File: `hu_SraRunInfo.csv`

Search Term:
```
"txid9606"[Organism:exp] AND ("type_rnaseq"[Filter]) AND cluster_public[prop] AND "platform illumina"[Properties]
```
SRA Accessed: `2020/05/30`

Results: `572215`

**AND** 

- File: `hu_meta_SraRunInfo.csv`

Search Term: 
```
"txid9606"[Organism:exp] AND ("metagenomic"[Filter] OR "metatranscriptomic"[Filter]) AND cluster_public[prop] AND "platform illumina"[Properties]
```
SRA Accessed: `2020/05/30`

Results: `36052` 


### Mouse

- File: `mu_SraRunInfo.csv`

Search Term:
```
("Mus musculus"[orgn]) AND ("type_rnaseq"[Filter] OR "metagenomic"[Filter] OR "metatranscriptomic"[Filter]) AND "platform illumina"[Properties]
```

SRA Accessed: `2020/06/06` 

Results: `594949`

### Mammalian

- File: `mamm_SraRunInfo.csv`

Search Term:
```
("Mammalia"[Organism] NOT "Homo sapiens"[Organism] NOT "Mus musculus"[orgn]) AND ("type_rnaseq"[Filter] OR "metagenomic"[Filter] OR "metatranscriptomic"[Filter]) AND "platform illumina"[Properties]
```
SRA Accessed: `2020/06/06`

Results: `82367`

### Vertebrate

- File: `vert_SraRunInfo.csv`

Search Term: 
```
("Vertebrata"[Organism] NOT "Mammalia"[Organism] NOT "Homo sapiens"[Organism] NOT "Mus musculus"[orgn]) AND ("type_rnaseq"[Filter] OR "metagenomic"[Filter] OR "metatranscriptomic"[Filter]) AND cluster_public[prop] AND "platform illumina"[Properties]
```

SRA Accessed: `2020/05/25`

Results: `86934`

### Virome

- File: `viro_SraRunInfo.csv`

Search Term: 
```
(virome) AND cluster_public[prop] AND "platform illumina"[Properties]
```

SRA Accessed: `2020/05/28`

Results: `8467`

### README Mirrors
- readme: `https://github.com/ababaian/serratus/wiki/SRA-queries`
- readme mirror: `s3://lovelywater/sra/README.md`
