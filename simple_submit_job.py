import json
import boto3
import sys

if len(sys.argv) < 4:
    exit("argument: [accession] [tag] [nb_threads]")

accession = sys.argv[1]
if "RR" not in accession:
    exit("accession should be of the form: [E/S]RR[0-9]+")

batch = boto3.client('batch')
#region = batch.meta.region_name
region = "us-east-1"

jobDefinition = 'RayanSerratusAssemblyHimemBatchJobDefinition'
#jobDefinition = 'RayanSerratusAssemblyBatchJobDefinition'

response = batch.submit_job(jobName='RayanSerratusAssemblyBatchJobQueue', 
                            jobQueue='RayanSerratusAssemblyBatchJobQueue', 
                            jobDefinition=jobDefinition, 
                            containerOverrides={
                                "command": [ "python", "simple_batch_processor.py"],
                                "environment": [ 
                                    {"name": "Accession", "value": accession},
                                    {"name": "Region", "value": region},
                                    {"name": "Tag", "value": sys.argv[2] },
                                    {"name": "Threads", "value": sys.argv[3] },
                                ]
                            })


print("Job ID is {}.".format(response['jobId']))
