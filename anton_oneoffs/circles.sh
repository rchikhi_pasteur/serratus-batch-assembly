while read acc f
do
	out=circles/"$acc"_"$(basename $f)"
	rm -f $out
	for k in `seq 18 200`
	do
		echo $f $k $out
	        python ../spades_circular_contigs/spades_circular_contigs.py $f $k >> $out
	done
done < <(python gather.py |grep -v "^#" | awk '{print $1" "$2}')
