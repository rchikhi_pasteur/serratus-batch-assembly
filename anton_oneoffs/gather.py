import glob,os
all_accs = set()
gene_clusters = set()
for filename in glob.glob("**",recursive=True):
    if filename.startswith("circles/"): continue
    if filename == "all.fasta": continue
    acc=None
    if filename.startswith("out_"):
        acc = filename.split('_')[1].split('.')[0]
        if "RR" not in acc: acc=None
    fs=filename.split('.')
    ext=fs[-1]
    if "fa" in ext:
        if acc is None and ("RR" in fs[-2] or "EU" in fs[-2]):
            acc=fs[-2].split('/')[0]
            if not ("RR" in acc or "EU" in acc):
                acc=fs[-2].split('/')[1]
            if not ("RR" in acc or "EU" in acc):
                acc=fs[-2].split('/')[2]
            if not ("RR" in acc or "EU" in acc):
                acc=None
        if acc is None and ("RR" in fs[-3] or "EU" in fs[-3]):
            acc=fs[-3].split('/')[-1]
        if acc is None or not ("RR" in acc or "EU" in acc):
            acc=None

        if "gene_cluster" in fs[-2]:
            gene_clusters.add((acc,filename))
            continue
        if "assembly_graph" in fs[-2]:
            continue
        if "raw_" in fs[-2]:
            continue
        
        #if acc is None: continue
        filesize = os.path.getsize(filename)
        print(acc,filename,filesize)
        all_accs.add(acc)

print("#",len(all_accs),"accessions")

print("# ----")
print("#",len(gene_clusters),"gene clusters:")
print('\n'.join(sorted([' '.join(map(str,x)) for x in gene_clusters])))
print("# intersect:",len(set([x[0] for x in gene_clusters]) & all_accs))
