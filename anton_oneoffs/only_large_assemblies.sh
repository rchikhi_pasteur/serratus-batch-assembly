rm -Rf staged
mkdir staged
while read acc f size
do
	ln -s $PWD/$f staged/$acc.spades.fasta
done < <(python gather.py|grep -v gene_clust |grep -v "^#" |awk '$3>1000000 {print;}' |grep RR)
