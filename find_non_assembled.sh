targetlist=lists/STAT_accessions.txt
targetlist=lists/CoV_id95-60_reads100p_score30p.unique.txt


targetlist=$HOME/sra_species_table/all_new_cov_nido.sra
folder=s3://serratus-public/assemblies/other/
assembler=coronaspades

targetlist=$HOME/sra_species_table/1000_random_sra.txt
folder=s3://serratus-public/assemblies/1krandom/other/
assembler=rnaviralspades

targetlist=lists/epsys_120.txt
folder=s3://serratus-public/assemblies/epsys_120_july21/other/
assembler=coronaspades

targetlist=lists/palmfold_5k_feb22.txt
folder=s3://serratus-public/assemblies/palmfold_5k_feb22/other/
assembler=rnaviralspades

#targetlist=lists/infernal_333_feb22.down_to_59.txt
targetlist=lists/infernal_333_feb22.19_I_thought_I_had.txt
folder=s3://serratus-public/assemblies/infernal_59_feb22/other/

targetlist=lists/infernal_28122022.txt
folder=s3://serratus-public/assemblies/infernal_dec22/other/

targetlist=lists/vanya_3k_28012023.new.txt
folder=s3://serratus-public/assemblies/vanya_3k_jan23/other/

if sort -C $targetlist; then
	:
else
	echo "warning: $targetlist isn't sorted"
fi

# this one compares just the existence of the folder (it may be empty)
#aws s3 ls $folder |grep $assembler |awk '{print $2}' |sed 's/\.coronaspades\///g' | sed 's/\.rnaviralspades-1krandom\///g'| sort  > find_non_assembled.assembled.txt
# this one checks for presence of scaffolds.fasta
aws s3 ls $folder --recursive |grep $assembler |grep scaffolds.fasta |awk '{print $4}' |cut -d"/" -f4|cut -d. -f1 |sort > find_non_assembled.assembled.txt
#aws s3 ls s3://serratus-public/assemblies/contigs/ |grep $assembler | awk '{print $4}' | sed "s/\.$assembler\.checkv_filtered\.fa//g" |sort > find_non_assembled.assembled.txt
#diff assembly_targets_first_1k.txt find_non_assembled.assembled.txt | grep "<" |awk '{print $2}' | tee find_non_assembled.txt
diff $targetlist find_non_assembled.assembled.txt | grep "<" |awk '{print $2}' | tee find_non_assembled.txt
